// Preloader
// jQuery(document).ready(function($) {
//     var Body = $('body');
//     Body.addClass('preloader-site');
// });
// $(window).load(function(){
//     $('.preloader-wrapper').fadeOut();
//     $('body').removeClass('preloader-site');
// });
$(document).ready(function() {
	$('body').addClass('loaded');
});
// Sticky Header
$(window).scroll(function () {
    var sc = $(window).scrollTop()
    if (sc > 300) {
        $(".site-header").addClass("sticky")
    } else {
        $(".site-header").removeClass("sticky")
    }
});

//On scroolTop selected Section
$(".navbar a").click(function(){
	$("body,html").animate({
	 scrollTop:$("#" + $(this).data('value')).offset().top
	},1000)
});
//On click change menu color
$(".navbar-nav").on("click", "li a", function () {
	$(".navbar-nav li a").removeClass("active");
	$(this).addClass("active");
});

/* Services Section */
 $('.serv-item').click(function(e){
	hideContentDivs();
	$('#service_item_1').hide();
	var tmp_div = $(this).index();
	$('.services div').eq(tmp_div).show();
	$('.service_item').eq(tmp_div).addClass('active');
 });

function hideContentDivs(){
	$('.services div').each(function(){
		$(this).hide();
		$('#service_item_1').show();
		$('.service_item').removeClass('active');
		
	});
}
hideContentDivs();

//Skillbars animation on load section
$('.skillbar').each(function(){
	$(this).find('.skillbar-bar').animate({
		width:$(this).attr('data-percent')
	},5000);
});

// init Isotope
var $grid = $('.portfolio-container').isotope({
	masonry: {
		gutter: 10,
	}
});
// filter items on button click
$('.portfolio-filter').on('click', 'li', function () {
	var filterValue = $(this).attr('data-filter');
	$grid.isotope({
		filter: filterValue
	});
});